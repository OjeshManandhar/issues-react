/* eslint-disable react-hooks/rules-of-hooks */
// packages
import React, { useState, useContext, useCallback } from 'react';

// types
import * as T from './types';

class Form<X> {
  private context = React.createContext<{
    state: X;
    setState: React.Dispatch<React.SetStateAction<X>>;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  }>(null!);

  // FormContainer: React.FunctionComponent<T.Props<X>> = props => {
  FormContainer: { (props: T.Props<X>): React.ReactElement } = props => {
    const [state, setState] = useState<X>(props.initialValue);

    return (
      <this.context.Provider value={{ state, setState }}>
        <form {...props.formProps} onSubmit={e => props.handleSubmit(e, state)}>
          {props.children}
        </form>
      </this.context.Provider>
    );
  };

  Input: {
    (
      props: React.InputHTMLAttributes<HTMLInputElement>
    ): React.ReactElement | null;
  } = props => {
    const name = props.name;
    if (!name) return null;

    const { state, setState } = useContext(this.context);

    const handleChange = useCallback(
      (e: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [name]: e.target.value });
      },
      [name, state, setState]
    );

    return (
      <input
        {...props}
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        value={(state as unknown as any)[name]}
        onChange={handleChange}
      />
    );
  };

  Submit: {
    (
      props: React.InputHTMLAttributes<HTMLInputElement>
    ): React.ReactElement | null;
  } = props => <input value='Submit' {...props} type='submit' />;
}

export default Form;
