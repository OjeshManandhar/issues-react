// packages
import type {
  FormEvent,
  ReactChild,
  ReactChildren,
  FormHTMLAttributes
} from 'react';

export type Props<T> = {
  initialValue: T;
  children:
    | ReactChild
    | ReactChildren
    | Array<ReactChild>
    | Array<ReactChildren>;
  formProps?: FormHTMLAttributes<HTMLFormElement>;
  handleSubmit: { (e: FormEvent<HTMLFormElement>, values: T): void };
};
