// packages
import styled from 'styled-components';

export const Button = styled.button`
  border-radius: 50%;
  border: 1px solid red;

  background-color: white;

  text-align: center;

  box-sizing: content-box;

  transition: all ease 300ms;

  cursor: pointer;

  &:hover {
    font-weight: 800;
    background-color: #eeeeee;
  }
`;

export const Form = {
  FormContainer: styled.form`
    margin-top: 1rem;

    font-size: 1.75rem;

    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: stretch;
  `,

  FormControl: styled.div`
    margin-bottom: 1rem;

    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;

    &:last-child {
      margin-bottom: 0rem;
    }
  `,

  Label: styled.label`
    width: 15rem;
    padding-top: 0.5rem;
    padding-right: 2rem;

    font-weight: 600;
    text-align: right;
  `,

  Input: styled.input`
    width: 50rem;
    padding: 0.5rem 1rem;

    font-size: 1.5rem;
  `,

  TextArea: styled.textarea`
    width: 50rem;
    height: 10rem;
    padding: 0.5rem 1rem;

    font-size: 1.5rem;

    resize: none;
  `,

  Submit: styled.input`
    align-self: center;

    font-size: 1.75rem;

    cursor: pointer;

    margin-top: 2rem;
    padding: 0.5rem 2.5rem;
  `
};
