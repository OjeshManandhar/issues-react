export enum ISSUE_STATUS {
  OPEN = 'open',
  ASSIGNED = 'assigned',
  CLOSED = 'closed'
}
