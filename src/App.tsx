// packages
import React, { useState, useEffect } from 'react';

// components
import Modal from 'components/Modal';
import Issues from 'components/Issues';
import Heading from 'components/Heading';
import Assignees from 'components/Assignees';
import IssueForm from 'components/IssueForm';
import AssigneeForm from 'components/AssigneeForm';

// models
import IssueModel from 'models/Issues';
import AssigneesModel from 'models/Assignees';

function App(): React.ReactElement {
  const [showIssueModal, setShowIssueModal] = useState<boolean>(false);
  const [showAssigneeModal, setShowAssigneeModal] = useState<boolean>(false);

  useEffect(() => {
    // Create dummy Assigne
    new AssigneesModel('Ojesh Manandhar', 'Backend Intern').save();

    // Create dummy Issue
    new IssueModel('Dummy', 'Just an issue', 'assignee-1').save();
  }, []);

  return (
    <React.Fragment>
      <Modal
        heading='Create Assignee'
        showModal={showAssigneeModal}
        closeModal={() => setShowAssigneeModal(false)}
      >
        <AssigneeForm closeModal={() => setShowAssigneeModal(false)} />
      </Modal>
      <Modal
        heading='Create Issue'
        showModal={showIssueModal}
        closeModal={() => setShowIssueModal(false)}
      >
        <IssueForm closeModal={() => setShowIssueModal(false)} />
      </Modal>

      <Heading />
      <Assignees showModal={() => setShowAssigneeModal(true)} />
      <Issues showModal={() => setShowIssueModal(true)} />
    </React.Fragment>
  );
}

export default App;
