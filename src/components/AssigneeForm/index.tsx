// packages
import React, { useState, useCallback } from 'react';

// Models
import Assignees from 'models/Assignees';

// styles
import { Form } from 'global/styles';

// types
import * as T from './types';

function AssigneeForm({ closeModal }: T.Props): React.ReactElement {
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      const assignee = new Assignees(name, description);
      assignee.save();

      closeModal();
    },
    [name, closeModal, description]
  );

  return (
    <Form.FormContainer
      action='#'
      name='assignee__form'
      autoComplete='off'
      onSubmit={handleSubmit}
    >
      <Form.FormControl>
        <Form.Label htmlFor='assigneeName'>Name</Form.Label>
        <Form.Input
          value={name}
          onChange={e => setName(e.target.value)}
          type='text'
          id='assigneeName'
          name='assigneeName'
          placeholder='Name of assignee'
          required
        />
      </Form.FormControl>

      <Form.FormControl>
        <Form.Label htmlFor='assigneeDescription'>Description</Form.Label>
        <Form.Input
          value={description}
          onChange={e => setDescription(e.target.value)}
          type='text'
          id='assigneeDescription'
          name='assigneeDescription'
          placeholder='Description of assignee (Optional)'
        />
      </Form.FormControl>

      <Form.Submit value='Submit' type='submit' />
    </Form.FormContainer>
  );
}

export default AssigneeForm;
