// packages
import React from 'react';

// styles
import * as S from './styles';

function Heading(): React.ReactElement {
  return <S.Heading>Issues React</S.Heading>;
}

export default Heading;
