// packages
import React, { useCallback, useState } from 'react';

// hooks
import useOptions from 'hooks/useOptions';

// models
import AssigneesModel from 'models/Assignees';

// global
import { ISSUE_STATUS } from 'global/enum';

// styles
import * as S from './styles';

// types
import * as T from './types';

function IssueBlock({ issue }: T.Props): React.ReactElement {
  const [newAssignee, setNewAssignee] = useState<string>('');
  const [showSaveBtn, setShowSaveBtn] = useState<boolean>(false);

  const options = useOptions();

  const data = issue.getData();

  const assigneeName =
    data.status === ISSUE_STATUS.ASSIGNED && data.assignee
      ? AssigneesModel.find(data.assignee)?.getData().name
      : null;

  const update = useCallback(() => {
    const assigneeId =
      options.find(op => op.props.value === newAssignee)?.props['data-value'] ||
      null;

    if (!assigneeId) {
      alert('Invalid input. Select an assignee from dropdown.');
      return;
    }

    issue.setAssignee(assigneeId);
  }, [issue, options, newAssignee]);

  return (
    <S.Block>
      <S.Title>{data.title}</S.Title>
      <S.Description>{data.description}</S.Description>

      {assigneeName ? (
        <S.Assigned>Assigned to {assigneeName}</S.Assigned>
      ) : data.status === ISSUE_STATUS.OPEN ? (
        <S.AssignTo>
          <S.Label htmlFor='issueAssignTo'>Assign to</S.Label>
          <S.Input
            value={newAssignee}
            onChange={e => {
              setShowSaveBtn(true);
              setNewAssignee(e.target.value);
            }}
            type='text'
            id='issueAssignTo'
            name='issueAssignTo'
            placeholder='Name of assignee'
            list={`assignee-name-list-${data.id}`}
          />
          <datalist id={`assignee-name-list-${data.id}`}>{options}</datalist>
        </S.AssignTo>
      ) : (
        <S.Assigned>Closed</S.Assigned>
      )}

      <S.Time>Created at {data.createdAt.toLocaleString()}</S.Time>

      {data.status === ISSUE_STATUS.OPEN && showSaveBtn && (
        <S.Update onClick={update}>Update</S.Update>
      )}
      {data.status !== ISSUE_STATUS.CLOSED && (
        <S.Close onClick={() => issue.close()}>Close</S.Close>
      )}
    </S.Block>
  );
}

export default IssueBlock;
