// packages
import styled from 'styled-components';

export const Block = styled.li`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  margin-bottom: 1rem;
  padding: 0.5rem 1rem;

  border: 1px solid red;

  &:last-child {
    margin-bottom: 0;
  }
`;

export const Title = styled.div`
  flex-grow: 1;

  font-weight: 700;
  font-size: 1.5rem;
`;

export const Description = styled.p`
  width: 100%;
  margin-top: 0.5rem;

  font-size: 1.25rem;
`;

export const Assigned = styled.div`
  font-weight: 700;
  font-size: 1.3rem;

  margin-top: 0.5rem;
`;

export const AssignTo = styled.div`
  width: 100%;
  font-size: 1.3rem;

  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const Label = styled.label`
  padding-right: 1rem;

  font-weight: bold;
`;

export const Input = styled.input`
  min-width: 25rem;
  padding: 0.25rem 1rem;

  font-size: 1.3rem;
`;

export const Time = styled.div`
  width: 100%;

  font-size: 1.3rem;

  margin-top: 0.5rem;
`;

export const Update = styled.button`
  margin-right: 1rem;
  margin-top: 0.5rem;

  padding: 0.25rem 1.25rem;

  cursor: pointer;
`;

export const Close = styled(Update)`
  margin-right: 0;
`;
