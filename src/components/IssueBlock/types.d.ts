// models
import Issues from 'models/Issues';

export type Props = {
  issue: Issues;
};
