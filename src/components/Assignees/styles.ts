// packages
import styled from 'styled-components';

export const Section = styled.section`
  margin: 1rem;
  padding: 1rem;

  border: 1px solid blue;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;

  margin-top: 1rem;

  overflow-x: auto;
`;
