// packages
import React, { useState, useEffect } from 'react';

// components
import Header from 'components/SectionHeader';
import AssigneeBlock from 'components/AssigneeBlock';

// models
import AssigneesModel, { ListenerFn } from 'models/Assignees';

// styles
import * as S from './styles';

// types
import * as T from './types';

function Assignees(props: T.Props): React.ReactElement {
  const [assignees, setAssignees] = useState<Array<AssigneesModel>>([]);

  useEffect(() => {
    const fn: ListenerFn = data => {
      setAssignees(data);
    };

    AssigneesModel.addListener(fn);

    return () => AssigneesModel.removeListener(fn);
  }, [setAssignees]);

  return (
    <S.Section>
      <Header heading='Assignees' onAddClick={props.showModal} />

      <S.List>
        {assignees.map(assignee => (
          <AssigneeBlock key={assignee.getData().id} assignee={assignee} />
        ))}
      </S.List>
    </S.Section>
  );
}

export default Assignees;
