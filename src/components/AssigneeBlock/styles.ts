// packages
import styled from 'styled-components';

// global
import { Button } from 'global/styles';

export const Block = styled.li`
  max-width: 20rem;
  min-width: 20rem;

  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  margin-right: 2rem;
  padding: 0.5rem 1rem;

  border: 1px solid red;

  &:last-child {
    margin-right: 0;
  }
`;

export const Name = styled.div`
  flex-grow: 1;
  max-width: calc(100% - 1.8rem);

  font-weight: 700;
  font-size: 1.5rem;

  text-overflow: ellipsis;

  /* Required for text-overflow to do anything */
  overflow: hidden;
  white-space: nowrap;
`;

export const Delete = styled(Button)`
  font-size: 1.5rem;

  width: 1.6rem;
  height: 1.6rem;

  line-height: 1.6rem;

  border: none;
`;

export const Description = styled.p`
  width: 100%;
  height: 1.7rem;

  margin-top: 0.5rem;

  font-size: 1.25rem;
  text-overflow: ellipsis;

  /* Required for text-overflow to do anything */
  overflow: hidden;
  white-space: nowrap;
`;
