// packages
import React from 'react';

// styles
import * as S from './styles';

// types
import * as T from './types';

function AssigneeBlock(props: T.Props): React.ReactElement {
  const data = props.assignee.getData();

  return (
    <S.Block>
      <S.Name>{data.name}</S.Name>
      <S.Delete onClick={() => props.assignee.delete()}>X</S.Delete>
      <S.Description>{data.description}</S.Description>
    </S.Block>
  );
}

export default AssigneeBlock;
