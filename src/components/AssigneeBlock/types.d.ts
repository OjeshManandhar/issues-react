// models
import Assignees from 'models/Assignees';

export type Props = {
  assignee: Assignees;
};
