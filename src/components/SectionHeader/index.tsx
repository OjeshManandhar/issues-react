// packages
import React from 'react';

// styles
import * as S from './styles';

// types
import * as T from './types';

function SectionHeader({ heading, onAddClick }: T.Props): React.ReactElement {
  return (
    <S.Container>
      <S.Heading>{heading}</S.Heading>
      <S.Add onClick={onAddClick}>+</S.Add>
    </S.Container>
  );
}

export default SectionHeader;
