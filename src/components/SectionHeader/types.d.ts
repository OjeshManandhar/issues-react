export type Props = {
  heading: string;
  onAddClick: () => void;
};
