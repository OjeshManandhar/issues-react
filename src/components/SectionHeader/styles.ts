// packages
import styled from 'styled-components';

// global
import { Button } from 'global/styles';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;

  & * {
    font-size: 2.5rem;
  }
`;

export const Heading = styled.h2`
  flex-grow: 1;
`;

export const Add = styled(Button)`
  width: 3rem;
  height: 3rem;

  line-height: 3rem;
`;
