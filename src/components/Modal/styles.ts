// packages
import styled from 'styled-components';

// global
import { Button } from 'global/styles';

export const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  opacity: 1;
  z-index: 100;

  overflow: hidden;

  background-color: rgba(0, 0, 0, 0.5);

  transition: all 500ms ease;
`;

export const Content = styled.div`
  position: relative;

  padding: 2rem;

  background-color: white;
`;

export const Heading = styled.h2`
  font-size: 2.5rem;
  text-align: center;
`;

export const Close = styled(Button)`
  position: absolute;
  top: 2rem;
  right: 2rem;

  font-size: 2rem;
  text-align: center;

  width: 2.5rem;
  height: 2.5rem;

  line-height: 2.5rem;
`;
