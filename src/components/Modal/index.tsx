// packages
import React from 'react';

// styles
import * as S from './styles';

// types
import * as T from './types';

function Modal(props: T.Props): React.ReactElement | null {
  if (!props.showModal) return null;

  return (
    <S.Backdrop>
      <S.Content>
        <S.Heading>{props.heading}</S.Heading>
        <S.Close onClick={props.closeModal}>X</S.Close>

        {props.children}
      </S.Content>
    </S.Backdrop>
  );
}

export default Modal;
