// packages
import type { ReactChild } from 'react';

export type Props = {
  heading: string;
  showModal: boolean;
  closeModal: () => void;
  children: ReactChild;
};
