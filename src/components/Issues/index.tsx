// packages
import React, { useState, useEffect } from 'react';

// components
import Header from 'components/SectionHeader';
import IssueBlock from 'components/IssueBlock';

// models
import IssuesModel, { ListenerFn } from 'models/Issues';

// styles
import * as S from './styles';

// types
import * as T from './types';

function Issues(props: T.Props): React.ReactElement {
  const [issues, setIssues] = useState<Array<IssuesModel>>([]);

  useEffect(() => {
    const fn: ListenerFn = data => {
      setIssues(data);
    };

    IssuesModel.addListener(fn);

    return () => IssuesModel.removeListener(fn);
  }, [setIssues]);

  return (
    <S.Section>
      <Header heading='Issues' onAddClick={props.showModal} />

      <S.List>
        {issues.map(issue => (
          <IssueBlock key={issue.getData().id} issue={issue} />
        ))}
      </S.List>
    </S.Section>
  );
}

export default Issues;
