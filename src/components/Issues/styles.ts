// packages
import styled from 'styled-components';

export const Section = styled.section`
  margin: 1rem;
  padding: 1rem;

  border: 1px solid blue;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;

  margin-top: 1rem;
`;
