// packages
import React, { useState, useEffect, useCallback } from 'react';

// model
import IssuesModel from 'models/Issues';
import AssigneesModel from 'models/Assignees';

// styles
import { Form } from 'global/styles';

// types
import * as T from './types';

function IssueForm({ closeModal }: T.Props): React.ReactElement {
  const [title, setTitle] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [assignee, setAssignee] = useState<string | null>(null);

  const [dataListOptions, setDataListOptions] = useState<Array<JSX.Element>>(
    []
  );

  const handleSubmit = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();

      const assigneeId =
        dataListOptions.find(op => op.props.value === assignee)?.props[
          'data-value'
        ] || null;

      const issue = new IssuesModel(title, description, assigneeId);
      issue.save();

      closeModal();
    },
    [title, assignee, closeModal, description, dataListOptions]
  );

  useEffect(() => {
    const data = AssigneesModel.load();

    const optionArr: Array<JSX.Element> = data.map(d => {
      const { id, name } = d.getData();

      return (
        <option key={id} data-value={id} value={name}>
          {name}
        </option>
      );
    });

    setDataListOptions(optionArr);
  }, [setDataListOptions]);

  return (
    <Form.FormContainer
      action='#'
      name='issue__form'
      autoComplete='off'
      onSubmit={handleSubmit}
    >
      <Form.FormControl>
        <Form.Label htmlFor='issueTitle'>Title</Form.Label>
        <Form.Input
          value={title}
          onChange={e => setTitle(e.target.value)}
          type='text'
          id='issueTitle'
          name='issueTitle'
          placeholder='Title of issue'
          required
        />
      </Form.FormControl>

      <Form.FormControl>
        <Form.Label htmlFor='issueDescription'>Description</Form.Label>
        <Form.TextArea
          value={description}
          onChange={e => setDescription(e.target.value)}
          id='issueDescription'
          name='issueDescription'
          placeholder='Description of issue (Optional)'
        ></Form.TextArea>
      </Form.FormControl>

      <Form.FormControl>
        <Form.Label htmlFor='issueAssignee'>Assignee</Form.Label>
        <Form.Input
          value={assignee ? assignee : ''}
          onChange={e => setAssignee(e.target.value)}
          type='text'
          id='issueAssignee'
          name='issueAssignee'
          placeholder='Name of assignee'
          list='assignee-name-list'
        />
        <datalist id='assignee-name-list'>{dataListOptions}</datalist>
      </Form.FormControl>

      <Form.Submit value='Submit' type='submit' />
    </Form.FormContainer>
  );
}

export default IssueForm;
