export type ListenerFn = (data: Array<Assignees>) => void;

class Assignees {
  private static newId = 1;
  private static list: Array<Assignees> = [];
  private static listenerList: Array<ListenerFn> = [];

  private id = '';
  private name = '';
  private description = '';

  constructor(name: string, description: string) {
    this.name = name;
    this.description = description;
  }

  private static callListeners(): void {
    Assignees.listenerList.forEach(fn => fn([...Assignees.list]));
  }

  public save(): Assignees {
    this.id = 'assignee-' + Assignees.newId++;

    Assignees.list.push(this);

    Assignees.callListeners();

    return this;
  }

  public delete(): void {
    const index = Assignees.list.findIndex((i: Assignees) => i.id === this.id);

    if (index !== -1) {
      Assignees.list.splice(index, 1);

      Assignees.callListeners();
    }
  }

  public getData(): { id: string; name: string; description: string } {
    return {
      id: this.id,
      name: this.name,
      description: this.description
    };
  }

  public static find(id: string): Assignees | undefined {
    return Assignees.list.find(a => a.id === id);
  }

  public static load(): Array<Assignees> {
    return [...Assignees.list];
  }

  public static addListener(fn: ListenerFn): void {
    Assignees.listenerList.push(fn);
  }

  public static removeListener(fn: ListenerFn): void {
    const index = Assignees.listenerList.findIndex(f => f === fn);

    if (index !== -1) {
      Assignees.listenerList.splice(index, 1);
    }
  }
}

export default Assignees;
