// global
import { ISSUE_STATUS } from 'global/enum';

export type ListenerFn = (data: Array<Issues>) => void;

class Issues {
  private static newId = 1;
  private static list: Array<Issues> = [];
  private static listenerList: Array<ListenerFn> = [];

  private id = '';
  private title = '';
  private description = '';
  private status = ISSUE_STATUS.OPEN;
  private assignee: string | null = null;
  private createdAt = new Date();

  constructor(
    title: string,
    description: string,
    assignee: string | null | undefined = null
  ) {
    this.title = title.trim();
    this.description = description.trim();
    if (assignee) {
      this.status = ISSUE_STATUS.ASSIGNED;
      this.assignee = assignee;
    } else {
      this.status = ISSUE_STATUS.OPEN;
      this.assignee = null;
    }
    this.createdAt = new Date();
  }

  private static callListeners(): void {
    Issues.listenerList.forEach(fn => fn([...Issues.list]));
  }

  public save(): Issues {
    this.id = 'issue-' + Issues.newId++;

    Issues.list.push(this);

    Issues.callListeners();

    return this;
  }

  public getData(): {
    id: string;
    title: string;
    description: string;
    status: ISSUE_STATUS;
    assignee: string | null;
    createdAt: Date;
  } {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      status: this.status,
      assignee: this.assignee,
      createdAt: this.createdAt
    };
  }

  public setAssignee(id: string): Issues {
    const index = Issues.list.findIndex((i: Issues) => i.id === this.id);

    this.status = ISSUE_STATUS.ASSIGNED;
    this.assignee = id;

    if (index !== -1) {
      Issues.list.splice(index, 1, this);

      Issues.callListeners();
    }

    return this;
  }

  public close(): void {
    const index = Issues.list.findIndex((i: Issues) => i.id === this.id);

    this.status = ISSUE_STATUS.CLOSED;

    if (index !== -1) {
      Issues.list.splice(index, 1, this);

      Issues.callListeners();
    }
  }

  public static find(id: string): Issues | undefined {
    return Issues.list.find(i => i.id === id);
  }

  public static load(): Array<Issues> {
    return [...Issues.list];
  }

  public static addListener(fn: ListenerFn): void {
    Issues.listenerList.push(fn);
  }

  public static removeListener(fn: ListenerFn): void {
    const index = Issues.listenerList.findIndex(f => f === fn);

    if (index !== -1) {
      Issues.listenerList.splice(index, 1);
    }
  }
}

export default Issues;
