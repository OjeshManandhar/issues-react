// packages
import React, { useState, useEffect } from 'react';

// models
import AssigneesModel, { ListenerFn } from 'models/Assignees';

function useOptions(): Array<JSX.Element> {
  const [options, setOptions] = useState<Array<JSX.Element>>([]);

  useEffect(() => {
    const fn: ListenerFn = data => {
      const options: Array<JSX.Element> = data.map(d => {
        const { id, name } = d.getData();

        return (
          <option key={id} data-value={id} value={name}>
            {name}
          </option>
        );
      });

      setOptions(options);
    };

    fn(AssigneesModel.load());

    AssigneesModel.addListener(fn);

    return () => AssigneesModel.removeListener(fn);
  }, [setOptions]);

  return options;
}

export default useOptions;
